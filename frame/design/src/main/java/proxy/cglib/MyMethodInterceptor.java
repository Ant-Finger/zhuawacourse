package proxy.cglib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * CGLIB动态代理
 *
 * @author zhibai
 */
public class MyMethodInterceptor implements MethodInterceptor {
    private final static Logger logger = LoggerFactory.getLogger(MyMethodInterceptor.class);

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        logger.info("You said: " + Arrays.toString(objects));
        return methodProxy.invokeSuper(o, objects);
    }
}
