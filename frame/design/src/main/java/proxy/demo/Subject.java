package proxy.demo;

/**
 * 抽象角色
 *
 * @author zhibai
 */
public interface Subject {
    void request();
}
