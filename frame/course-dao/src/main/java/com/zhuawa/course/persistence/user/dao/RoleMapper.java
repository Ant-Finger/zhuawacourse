package com.zhuawa.course.persistence.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhuawa.course.persistence.user.entity.Role;


public interface RoleMapper extends BaseMapper<Role> {
}
