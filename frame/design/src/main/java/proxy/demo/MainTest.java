package proxy.demo;

/**
 * 客户端调用
 *
 * @author zhibai
 */
public class MainTest {
    public static void main(String[] args) {
        Subject subject = new ProxySubject();
        //代理者代替真实者做事情
        subject.request();
    }
}
