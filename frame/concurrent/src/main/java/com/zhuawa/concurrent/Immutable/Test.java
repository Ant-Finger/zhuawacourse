package com.zhuawa.concurrent.Immutable;

public class Test {
    public static void main(String[] args) {
        Person jack = new Person();
        jack.setAge(101);
        jack.setIdentityCardID("42118220090315234X");

        System.out.println(validAge(jack));
        // 后续使用可能没有察觉到jack的age被修改了
        // 为后续埋下了不容易察觉的问题
    }

    public static boolean validAge(Person person) {
        if (person.getAge() >= 100) {
            person.setAge(100);  // 此处产生了副作用
            return false;
        }
        return true;
    }

}
