package proxy.jdk;

import proxy.demo.RealSubject;
import proxy.demo.Subject;

import java.lang.reflect.Proxy;

/**
 * 访问动态代理
 *
 * @author zhibai
 */
public class MainTest {
    public static void main(String[] args) {
        //该设置用于输出jdk动态代理产生的类
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");

        Subject realSubject = new RealSubject();
        DynamicProxy proxy = new DynamicProxy(realSubject);
        ClassLoader classLoader = realSubject.getClass().getClassLoader();
        Subject subject = (Subject) Proxy.newProxyInstance(classLoader, new Class[]{Subject.class}, proxy);
        subject.request();
    }
}
