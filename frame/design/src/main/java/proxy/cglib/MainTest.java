package proxy.cglib;

import org.springframework.cglib.proxy.Enhancer;

/**
 * 访问CGLIB动态代理获取代理对象
 *
 * @author zhibai
 */
public class MainTest {

    public static void main(String[] args) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(HelloConcrete.class);
        enhancer.setCallback(new MyMethodInterceptor());

        HelloConcrete hello = (HelloConcrete) enhancer.create();
        System.out.println(hello.sayHello("I love you!"));
    }
}
