package proxy.demo;

/**
 * 静态代理，对具体真实对象直接引用
 * 代理角色，代理角色需要有对真实角色的引用，
 * 代理做真实角色想做的事情
 *
 * @author zhibai
 */
public class ProxySubject implements Subject {

    private RealSubject realSubject = null;

    /**
     * 除了代理真实角色做该做的事情，代理角色也可以提供附加操作，
     * 如：preRequest()和postRequest()
     */
    @Override
    public void request() {
        //真实角色操作前的附加操作
        preRequest();

        if (realSubject == null) {
            realSubject = new RealSubject();
        }
        realSubject.request();
        //真实角色操作后的附加操作
        postRequest();
    }

    /**
     * 真实角色操作前的附加操作
     */
    private void postRequest() {
        System.out.println("访问真实主题之后的后续处理。");
    }

    /**
     * 真实角色操作后的附加操作
     */
    private void preRequest() {
        System.out.println("访问真实主题之前的预处理。");
    }
}
