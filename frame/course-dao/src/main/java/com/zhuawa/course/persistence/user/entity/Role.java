package com.zhuawa.course.persistence.user.entity;

import lombok.Data;


@Data
public class Role {
    private Long id;
    private String roleName;
    private String roleDescribe;
}
